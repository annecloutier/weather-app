FROM python:3.8-alpine3.10
# TODO: Copy application files into container
COPY * /weather-app/

RUN pip3 install -r /weather-app/requirements.txt

CMD python3 /weather-app/weather.py

# docker build -t weather:v1 .
# docker run --name weather_container_v1 weather:v1